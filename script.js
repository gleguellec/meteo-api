/* Utilisation de l'API OpenWeatherMap 

Example of API call:
https://api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=d99ce47088f58048706c80a36a5cce03

*/

function search(){
    let input = document.getElementById('city').value;
    url = "https://api.openweathermap.org/data/2.5/weather?q="+input+"&APPID=d99ce47088f58048706c80a36a5cce03";
    getJson(url);
}

function getJson(url){
    fetch(url)
    .then(function(res) {
        if (res.ok) {
            return res.json();
        }
    })
    .then(function(value) {
        //console.log(value);//données en json
        let city = value.name;
        let country = value.sys.country;
        //let weather = value.weather[0].description;
        let icon = value.weather[0].icon;
        let iconImg = "https://openweathermap.org/img/wn/" + icon + "@2x.png";
        let windSpeed = Math.floor(value.wind.speed);
        let temperatureKelvin = value.main.temp;
        let temperatureCelsius = Math.floor(temperatureKelvin - 273.15);
        let humidity = value.main.humidity;
        document.getElementById("return").innerHTML = "<h2>" + city + ", " + country + "</h2><p>" + "<img src='" + iconImg + "'>" //+ weather
                                                    + "<br>Température : " + temperatureCelsius + "°C"
                                                    + "<br> Humidité : " + humidity + " %"
                                                    + "<br> Vitesse du vent : " + windSpeed + " km/h"
                                                    +"</p>";
    })
    .catch(function(err) {
        console.error("Une erreur est survenue");
        document.getElementById("return").innerHTML = "<p>Entrez un nom de commune valide</p>";
    });
}